-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 13 fév. 2020 à 21:56
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ccd_jcvd`
--

-- --------------------------------------------------------

--
-- Structure de la table `besoin`
--

CREATE TABLE `besoin` (
  `idBesoin` int(11) NOT NULL,
  `idCreneau` int(11) NOT NULL,
  `idPoste` int(11) NOT NULL,
  `idBenev` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `login` varchar(50) NOT NULL,
  `mdp` varchar(200) NOT NULL,
  `admin` int(1) NOT NULL,
  `idBenev` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`login`, `mdp`, `admin`, `idBenev`) VALUES
('brigitte', 'skurt', 0, 1),
('root', 'root', 1, 8);

-- --------------------------------------------------------

--
-- Structure de la table `creneau`
--

CREATE TABLE `creneau` (
  `jour` int(5) NOT NULL,
  `semaine` int(5) NOT NULL,
  `heureDeb` int(5) NOT NULL,
  `heureFin` int(5) NOT NULL,
  `idCreneau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `idPoste` int(11) NOT NULL,
  `label` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`idPoste`, `label`) VALUES
(1, 'Caissier titulaire'),
(2, 'Caissier assistant'),
(3, 'Gestionnaire de vrac titulaire'),
(4, 'Gestionnaire de vrac assistant'),
(5, 'Chargé d\'accueil titulaire'),
(6, 'Chargé d\'accueil assistant');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `idBenev` int(11) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `age` int(5) NOT NULL,
  `adresse` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idBenev`, `prenom`, `email`, `image`, `nom`, `age`, `adresse`) VALUES
(1, 'Cassandre', 'cassandre@gmail.com', '1.jpg', 'Michalac', 0, ''),
(2, 'Achille', 'achille@gmail.com', '2.jpg', 'lozkd', 0, ''),
(3, 'Calypso', 'calypso@gmail.com', '3.jpg', 'Pdhad', 0, ''),
(4, 'Bacchus', 'bacchus@gmail.com', '4.jpg', 'apzdoazdo', 0, ''),
(5, 'Diane', 'diane@gmail.com', '5.jpg', 'zaezaezae', 0, ''),
(6, 'Clark', 'clark@gmail.com', '6.jpg', 'fezfezfz', 0, ''),
(7, 'Helene', 'helene@gmail.com', '7.jpg', 'zdsdcc', 0, ''),
(8, 'Jason', 'jason@gmail.com', '8.jpg', 'ololjl', 0, ''),
(9, 'Bruce', 'bruce@gmail.com', '9.jpg', 'ervefv', 0, ''),
(10, 'Pénélope', 'penelope@gmail.com', '10.jpg', 'pziehzenc', 0, ''),
(11, 'Ariane', 'ariane@gmail.com', '11.jpg', 'pazizen', 0, ''),
(12, 'Lois', 'lois@gmail.com', '12.jpg', 'bezgcyzen', 0, '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `besoin`
--
ALTER TABLE `besoin`
  ADD PRIMARY KEY (`idBesoin`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`login`,`idBenev`),
  ADD KEY `fk_compte_user` (`idBenev`);

--
-- Index pour la table `creneau`
--
ALTER TABLE `creneau`
  ADD PRIMARY KEY (`idCreneau`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`idPoste`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idBenev`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `besoin`
--
ALTER TABLE `besoin`
  MODIFY `idBesoin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `creneau`
--
ALTER TABLE `creneau`
  MODIFY `idCreneau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `idPoste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `idBenev` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `fk_compte_user` FOREIGN KEY (`idBenev`) REFERENCES `user` (`idBenev`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
