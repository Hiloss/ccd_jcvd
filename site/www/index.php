<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Autoloader
 */
require_once '../src/vendor/autoload.php';
session_start();
if(!isset($_SESSION['estConnecte'])){
$_SESSION['estConnecte']=false;
}
if(!isset($_SESSION['estAdmin'])){
    $_SESSION['estAdmin']=false;
}
/**
 * Création connection avec la DB
 */
new coboard\db\Connection();

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

/**
 * L'affichage d'une page d'accueil
 */


if($_SESSION['estConnecte']){
    $app->get('/',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\UserControler();
            return $lc->getDefault($req, $resp, $args);
        }
    )->setName('route_index');

    if($_SESSION['estAdmin']){

        $app->get('/besoins/creation/{idCreneau}',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\BesoinControler();
                return $lc->formBesoin($req, $resp, $args);
            }
        )->setName('route_creation_besoin');

        $app->get('/besoins/reserver/{idBesoin}',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\BesoinControler();
                return $lc->formReserv($req, $resp, $args);
            }
        )->setName('route_reserv_besoin');

        $app->post('/besoins/reserver/{idBesoin}',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\BesoinControler();
                return $lc->reserver($req, $resp, $args);
            }
        )->setName('route_reserv_besoin_post');

        $app->post('/besoins/creation/{idCreneau}',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\BesoinControler();
                return $lc->creationBesoin($req, $resp, $args);
            }
        )->setName('route_creation_besoin_post');

        $app->get('/creneau',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\CreneauControler();
                return $lc->getAffichageFormCreation($req, $resp, $args);
            }
        )->setName('route_creation_creneau');

        $app->post('/creneau',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\CreneauControler();
                return $lc->creerCreneau($req, $resp, $args);
            }
        )->setName('route_creation_creneau_post');

        $app->get('/creneau/supprimer/{id}',
            function (Request $req, Response $resp, $args) {
                $lc = new \coboard\controler\CreneauControler();
                return $lc->supprimerCreneau($req, $resp, $args);
            }
        )->setName('route_supprCreneau');
    }

    $app->get('/user',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\UserControler();
            return $lc->getUsers($req, $resp, $args);
        }
    )->setName('route_listeUsers');

    $app->get('/user/perso/{u}',function (Request $req, Response $resp, $args){
        $uc = new \coboard\controler\UserControler();
        $uc->getUser($req,$resp,$args['u']);
    });

    $app->get('/user/login',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\CompteControler();
            return $lc->getAffichageLogin($req, $resp, $args);
        }
    )->setName('route_loginUsers');

    $app->post('/user/login',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\CompteControler();
            return $lc->tryCon($req, $resp, $args);
        }
    )->setName('route_loginuserPost');

    $app->get('/user/logout',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\CompteControler();
            return $lc->getLogout($req, $resp, $args);
        }
    )->setName('route_logoutUsers');


    $app->get('/besoins/{idCreneau}',
        function (Request $req, Response $resp, $args) {
            $lc = new \coboard\controler\BesoinControler();
            return $lc->getAffichageBesoin($req, $resp, $args);
        }
    )->setName('route_getBesoin');
}
else
{
    $app->get('/',
    function (Request $req, Response $resp, $args) {
      return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
    }
)->setName('route_index');


$app->get('/creneau',
    function (Request $req, Response $resp, $args) {
        return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
    }
)->setName('route_creation_creneau');

$app->post('/creneau',
    function (Request $req, Response $resp, $args) {
        return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
    }
)->setName('route_creation_creneau_post');

$app->get('/user',
    function (Request $req, Response $resp, $args) {
        return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
    }
)->setName('route_listeUsers');



$app->get('/user/perso/{u}',function (Request $req, Response $resp, $args){
    return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
});



$app->get('/besoins/{idCreneau}',
    function (Request $req, Response $resp, $args) {
        return $resp->withRedirect($req->getUri()->getBasePath()."/user/login",301);
    }

)->setName('route_loginUsers');

$app->get('/user/login',
    function (Request $req, Response $resp, $args) {
        $lc = new \coboard\controler\CompteControler();
        return $lc->getAffichageLogin($req, $resp, $args);
    }
)->setName('route_loginUsers');

$app->post('/user/login',
    function (Request $req, Response $resp, $args) {
        $lc = new \coboard\controler\CompteControler();
        return $lc->tryCon($req, $resp, $args);
    }
)->setName('route_loginuserPost');
}
$app->run();