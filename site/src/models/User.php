<?php
namespace coboard\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'idBenev';
    public $timestamps = false;
}