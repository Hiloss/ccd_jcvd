<?php
namespace coboard\models;

class Role extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'role';
    protected $primaryKey = 'id';
    public $timestamps = false;
}