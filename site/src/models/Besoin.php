<?php
namespace coboard\models;

class Besoin extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'besoin';
    protected $primaryKey = 'idBesoin';
    public $timestamps = false;

    public function creneau() {
        return $this->belongsTo('coboard\models\Creneau','idCreneau','idCreneau');
    }
    public function role() {
        return $this->belongsTo('coboard\models\Role','idPoste','idPoste');
    }
}