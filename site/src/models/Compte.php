<?php
namespace coboard\models;

class Compte extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'compte';
    protected $primaryKey = 'login';
    public $timestamps = false;
    public function user() {
        return $this->belongsTo('coboard\models\User','idBenev','idBenev');
    }
}
