<?php
namespace coboard\models;

class Creneau extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'creneau';
    protected $primaryKey = 'idCreneau';
    public $timestamps = false;
}