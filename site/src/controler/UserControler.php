<?php
namespace coboard\controler;

use coboard\models\Besoin;
use coboard\models\User;
use coboard\models\Role;
use coboard\vue\VueCreneau;
use coboard\vue\VueGlobale;
use coboard\vue\VueUsers;
use coboard\models\Creneau;
class UserControler
{
    public function getUsers( $rq, $rs, $args){
        $Users = User::get();
        $vue = new \coboard\vue\VueUsers($Users,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(0));
        return $rs;
    }

    public function getDefault($rq, $rs, $args){
        $vue = new \coboard\vue\VueGlobale(null,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(0));
        return $rs;
    }


    public function getUser($req,$resp,$args){
        $us=Besoin::where('idBenev','=',$args)->get();
        $v=new \coboard\vue\VueUsers($us,$req->getUri()->getBasePath());
        $resp->getBody()->write($v->render(1));
    }



}
