<?php
namespace coboard\controler;

use coboard\models\User;
use coboard\models\Role;
use coboard\models\Creneau;
use coboard\models\Besoin;
class CreneauControler
{
    public function getAffichageFormCreation( $rq, $rs, $args){
        $creneau = Creneau::orderBy('jour',ASC)->get();
        $vue = new \coboard\vue\VueCreneau($creneau,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(1));
        return $rs;
    }

    public function creerCreneau( $rq, $rs, $args){
        $cren = new Creneau();
        $jour = filter_var($_POST['jour'], FILTER_VALIDATE_INT);
        $heureD = filter_var($_POST['hdebut'], FILTER_VALIDATE_INT);
        $heureF = filter_var($_POST['hfin'], FILTER_VALIDATE_INT);
        $sem = filter_var($_POST['semaine'], FILTER_VALIDATE_INT);
        //Verification heure
        $creneau = Creneau::where('jour','=',$jour)->get();
        $creOk = true;
        foreach($creneau as $cre){
            if($creOk) {
                if (!(((($heureD < $cre->heureDeb) && ($heureF <= $cre->heureDeb)) || ($heureD >= $cre->heureFin)) && ($heureD < $heureF))) {
                    $creOk = false;
                }
            }
        }
        $cren->jour = $jour;
        $cren->semaine = $sem;
        $cren->heureDeb = $heureD;
        $cren->heureFin = $heureF;
        if($creOk) {
            $cren->save();
        }
        $creneau = Creneau::orderBy('jour','ASC')->get();
        $vue = new \coboard\vue\VueCreneau($creneau,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(1));
        return $rs;
    }

    public function supprimerCreneau( $rq, $rs, $args){
        $id = $args['id'];
        $creneau = Creneau::where('idCreneau','=',$id)->first();
        foreach ($creneau->idBesoin as $idB){
            $besoin = Besoin::where('idBesoin','=',$idB)->first();
            $besoin->delete();
        }

        $besoins = Besoin::where('idCreneau', '=',$id)->get();
        foreach ($besoins as $bes){
            $bes->delete();
        }
        $creneau->delete();
        return $rs->withRedirect($rq->getUri()->getBasePath() . "/creneau",301);
    }
}
