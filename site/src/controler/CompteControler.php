<?php
namespace coboard\controler;

use coboard\models\User;
use coboard\models\Role;
use coboard\models\Compte;
use coboard\vue\VueGlobale;

class CompteControler
{

    public function getAffichageLogin($rq, $rs, $args){
     if($_SESSION['estConnecte']){
        return $rs->withRedirect($rq->getUri()->getBasePath()."/",301);
     }
     else{
        $vue = new \coboard\vue\VueCompte(null,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(0));
        return $rs;
     }
       
    }

    public function getLogout($rq, $rs, $args){
        session_destroy();
        return $rs->withRedirect($rq->getUri()->getBasePath()."/",301);
    }

    public function tryCon($rq, $rs, $args){
        $log = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
        $usr = Compte::where('login','=',$log)->first();
     
      
        if($usr){
            $mdp = filter_var($_POST['pwd'], FILTER_SANITIZE_STRING);
         
            if($mdp == $usr->mdp){
                
                $_SESSION['estConnecte'] = true;
                $_SESSION['login'] = $log;
                if($usr->admin == 0){
                    $_SESSION['estAdmin'] = 0;
                }else{
                    $_SESSION['estAdmin'] = 1;
                }
                return $rs->withRedirect($rq->getUri()->getBasePath()."/",301);
            }
            
        }
        else
        {
            echo "<h2>MAUVAIS LOGIN OU PASSWORD</h2>";
        }
    }
}