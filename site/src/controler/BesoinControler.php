<?php
namespace coboard\controler;

use coboard\models\Besoin;
use coboard\models\User;
use coboard\models\Role;
use coboard\models\Compte;
use coboard\vue\VueGlobale;

class BesoinControler{
    public function getAffichageBesoin($rq, $rs, $args){
        $id = $args['idCreneau'];
        $besoin = Besoin::where("idCreneau",'=',$id)->get();
        $vue = new \coboard\vue\VueBesoin($besoin,$rq->getUri()->getBasePath(),$id);
        $rs->getBody()->write($vue->render(0));
        return $rs;
    }

    public function formBesoin($rq, $rs, $args){
        $id = $args['idCreneau'];
        $vue = new \coboard\vue\VueBesoin(null,$rq->getUri()->getBasePath(),$id);
        $rs->getBody()->write($vue->render(1));
        return $rs;
    }

    public function formReserv($rq, $rs, $args){
        $id = $args['idBesoin'];
        $vue = new \coboard\vue\VueBesoin(null,$rq->getUri()->getBasePath(),$id);
        $rs->getBody()->write($vue->render(2));
        return $rs;
    }



    public function creationBesoin($rq, $rs, $args){
        $id = $args['idCreneau'];
        $poste = $_POST['poste'];

        $besoin = new Besoin();
        $num = 0;
        switch($poste){
            case 'Caissier titulaire':
                $num = 1;
                break;
            case 'Caissier assistant':
                $num = 2;
                break;
            case 'Gestionnaire de vrac titulaire':
                $num = 3;
                break;
            case 'Gestionnaire de vrac assistant':
                $num = 4;
                break;
            case 'Chargé d\'accueil titulaire':
                $num = 5;
                break;
            case 'Chargé d\'accueil assistant':
                $num = 6;
                break;
        }
        $besoin->idPoste = $num;
        $besoin->idBenev = 0;
        $besoin->idCreneau = $id;

        $besoin->save();
        return $rs->withRedirect($rq->getUri()->getBasePath() . "/besoins/$id",301);
    }

    public function reserver($rq, $rs, $args){
        $id = $args['idBesoin'];
        $poste = $_POST['prenom'];

        $besoin = Besoin::where('idBesoin', '=', $id)->first();
        $num = 0;
        switch($poste){
            case 'Cassandre':
                $num = 1;
                break;
            case 'Achille':
                $num = 2;
                break;
            case 'Calypso':
                $num = 3;
                break;
            case 'Bacchus':
                $num = 4;
                break;
            case 'Diane':
                $num = 5;
                break;
            case 'Clark':
                $num = 6;
                break;
            case 'Helene':
                $num = 7;
                break;
            case 'Jason':
                $num = 8;
                break;
            case 'Bruce':
                $num = 9;
                break;
            case 'Pénélope':
                $num = 10;
                break;
            case 'Ariane':
                $num = 11;
                break;
            case 'Lois':
                $num = 12;
                break;
        }
        $besoin->idBenev = $num;

        $besoin->save();
        return $rs->withRedirect($rq->getUri()->getBasePath() . "/creneau",301);
    }

}