<?php
namespace coboard\vue;


class VueGlobale
{

    protected $tab;
    /**
     * @var string
     * base path de la requete
     */
    protected $basePath;
    protected $error;


    public function __construct($t,$b)
    {
        $this->tab = $t;
        $this->basePath = $b;
        $this->error = '';
    }

    /**
     * @param $selec int Selection de la vue
     * @return string html a afficher
     */
    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 0 :
            {
                $content .= $this->index();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage d'une liste de souhait
     */
    public function index(){
        $res="<img src=\"$this->basePath/img/aliments.png\" width=100%/>";
        /*
        $b=$this->tab;


        if (isset($b)){
            $res .= "<p style =\"font-size: 2em\">Nous n'avons plus rien à vous proposer pour le moment, repasser plus tard.</p>";
        }else {
            $res .= "Nous avons besoin de vous pour : ";
            foreach ($b as $label) {
                $res .= "<p style =\"font-size: 2em\">     *   $label->label<a href=\"#\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-chevron-right\"></span> Faire cette tâche</a></>";
            }
        }
*/
        return $res;

    }

    public function entete(){
        $res="
<!DOCTYPE html>
<html lang='en'>

<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  <meta name='description' content=''>
  <meta name='author' content='Dashboard'>
  <meta name='keyword' content='Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina'>
  <title>CoBoard</title>

  <!-- Favicons -->
  <link href='".$this->basePath."/img/favicon.png' rel='icon'>
  <link href='".$this->basePath."/img/apple-touch-icon.png' rel='apple-touch-icon'>

  <!-- Bootstrap core CSS -->
  <link href='".$this->basePath."/lib/bootstrap/css/bootstrap.min.css' rel='stylesheet'>
  <!--external css-->
  <link href='".$this->basePath."/lib/font-awesome/css/font-awesome.css' rel='stylesheet' />
  <link rel='stylesheet' type='text/css' href='".$this->basePath."/css/zabuto_calendar.css'>
  <link rel='stylesheet' type='text/css' href='".$this->basePath."/lib/gritter/css/jquery.gritter.css' />
  <!-- Custom styles for this template -->
  <link href='".$this->basePath."/css/style.css' rel='stylesheet'>
  <link href='".$this->basePath."/css/style-responsive.css' rel='stylesheet'>
  <script src='".$this->basePath."/lib/chart-master/Chart.js'></script>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>";
if($_SESSION['estConnecte']){
  $res.="

<body>
  <section id='container'>
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class='header black-bg'>
      <div class='sidebar-toggle-box'>
        <div class='fa fa-bars tooltips' data-placement='right' data-original-title='Barre de navigation'></div>
      </div>
      <!--logo start-->
      <a href='".$this->basePath."' class='logo'><b><span>CoBoard</span></b></a>
      <!--logo end-->
      <div class='top-menu'>

      </div>
    </header>
    
 
    <aside>

      <div id='sidebar' class='nav-collapse '>
        <!-- sidebar menu start-->
        <ul class='sidebar-menu' id='nav-accordion'>
          <p class='centered'><a href='".$this->basePath."'><img src='".$this->basePath."/img/geglogo.jpg' class='img-circle' width='80'></a></p>
          <h5 class='centered'>GRANDE EPICERIE GENERALE</h5>
          <li class='mt'>
            <a class='active' href='".$this->basePath."'>
              <i class='fa fa-dashboard'></i>
              <span>Tableau de bord</span>
              </a>
          </li>
";
    if($_SESSION['estAdmin']){
        $res .= "<li class='mt'>
            <a class='active' href='".$this->basePath."/creneau'>
              <i class='fa fa-dashboard'></i>

              <span>Création créneaux</span>
              </a>
          </li>";
    }
    $res .= "
          <li class='mt'>
            <a class='active' href='".$this->basePath."/user'>
              <i class='fa fa-dashboard'></i>
              <span>Liste bénévole</span>
              </a>
          </li>
      
     
          <li class='mt'>
            <a class='active' href='".$this->basePath."/user/logout'>
              <i class='fa fa-dashboard'></i>
              <span>Se Deconnecter</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id='main-content'>
      <section class='wrapper site-min-height'>";
}
return $res;
    }

    public function bas(){
      $res="";
      if($_SESSION['estConnecte']){
        
        $res.="
        </section>
              <!-- /wrapper -->
            </section>
            <!-- /MAIN CONTENT -->
            <!--main content end-->
            <!--footer start-->
            <footer class='site-footer'>
              <div class='text-center'>
                <p>
                  &copy; Copyrights <strong>COBoard</strong>. All Rights Reserved
                </p>
                <div class='credits'>
                  <!--
                    You are NOT allowed to delete the credit link to TemplateMag with free version.
                    You can delete the credit link only if you bought the pro version.
                    Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
                    Licensing information: https://templatemag.com/license/
                  -->
                  Created with Dashio template by <a href='https://templatemag.com/'>TemplateMag</a>
                </div>
              </div>
            </footer>
          </section>
          <!-- js placed at the end of the document so the pages load faster -->
          <script src='".$this->basePath."/lib/jquery/jquery.min.js'></script>

          <script src='".$this->basePath."/lib/bootstrap/js/bootstrap.min.js'></script>
          <script class='include' type='text/javascript' src='".$this->basePath."/lib/jquery.dcjqaccordion.2.7.js'></script>
          <script src='".$this->basePath."/lib/jquery.scrollTo.min.js'></script>
          <script src='".$this->basePath."/lib/jquery.nicescroll.js' type='text/javascript'></script>
          <script src='".$this->basePath."/lib/jquery.sparkline.js'></script>
          <!--common script for all pages-->
          <script src='".$this->basePath."/lib/common-scripts.js'></script>
          <script type='text/javascript' src='".$this->basePath."/lib/gritter/js/jquery.gritter.js'></script>
          <script type='text/javascript' src='".$this->basePath."/lib/gritter-conf.js'></script>
          <!--script for this page-->
          <script src='".$this->basePath."/lib/sparkline-chart.js'></script>
          <script src='".$this->basePath."/lib/zabuto_calendar.js'></script>
        </body>

        </html>";
                    }
       return $res;
    }


    /**
     * @param $msg
     * @return string
     * Retourne une erreur
     */
    public function error($msg){
        return $this->entete() . $msg . $this->bas();
    }
}