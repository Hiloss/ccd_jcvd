<?php
namespace coboard\vue;


class VueCreneau extends VueGlobale
{
     public function __construct($t,$b){
         parent::__construct($t,$b);
     }

    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 1 :
            {
                $content .= $this->affichageCreneau();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    public function affichageCreneau(){
         $res = "
<h1>Gestion des créneaux</h1>
<table class=\"table\">
  <thead>
    <tr>
      <th scope=\"col\">Jour</th>
      <th scope=\"col\">Heure Début</th>
      <th scope=\"col\">Heure Fin</th>
      <th scope=\"col\">Listes des besoins</th>
      <th scope=\"col\">Suppression</th>
    </tr>
  </thead>
  <tbody>";
        foreach ($this->tab as $creneau) {
            $j = "";
            switch($creneau->jour){
                case 1:
                    $j = "Lundi";
                    break;
                case 2:
                    $j = "Mardi";
                    break;
                case 3:
                    $j = "Mercredi";
                    break;
                case 4:
                    $j = "Jeudi";
                    break;
                case 5:
                    $j = "Vendredi";
                    break;
                case 6:
                    $j = "Samedi";
                    break;
                case 7:
                    $j = "Dimanche";
                    break;
            }
            $res .= "<tr>
<th scope=\"row\">$j</th>
      <td>$creneau->heureDeb</td>
      <td>$creneau->heureFin</td>
      <td><button onclick=\"location.href='$this->basePath/besoins/$creneau->idCreneau'\" type=\"button\" class=\"btn btn-success\">Voir besoins pour ce créneau</button></td>
      <td><button onclick=\"location.href='$this->basePath/creneau/supprimer/$creneau->idCreneau'\" type=\"button\" class=\"btn btn-danger\">Supprimer</button></td>
</tr>";
        }

        $res .= "</tbody>
</table>
<br>
<form action=\"$this->basePath/creneau\" method=\"post\">
  <div class=\"form-group\">
    <label for=\"jour\">Jour</label>
    <input type=\"texte\" class=\"form-control\" id=\"jour\" name=\"jour\" placeholder=\"Numéro de jour\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"semaine\">Semaine</label>
    <input type=\"texte\" class=\"form-control\" id=\"semaine\" name=\"semaine\" placeholder=\"Lettre de semaine\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"hdebut\">Heure de début</label>
    <input type=\"texte\" class=\"form-control\" id=\"hdebut\" name=\"hdebut\" placeholder=\"Heure de début\" required>
  </div>
  <div class=\"form-group\">
    <label for=\"hdebut\">Heure de fin</label>
    <input type=\"texte\" class=\"form-control\" id=\"hfin\" name=\"hfin\" placeholder=\"Heure de fin\" required>
  </div>
  <button type=\"submit\" class=\"btn btn-primary\">Valider</button>
</form>";
         return $res;
    }
}