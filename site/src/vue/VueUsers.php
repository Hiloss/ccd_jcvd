<?php
namespace coboard\vue;

use coboard\models\Creneau;
class VueUsers extends VueGlobale
{

    public function __construct($t,$b){
        parent::__construct($t,$b);
    }


    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 0 :
            {
                $content .= $this->affichagePanelUsers();
                break;
            }
            case 1 :
            {
                $content .=$this->affichageUser();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }



    public function affichagePanelUsers(){
        $i=0;
        $res='';
        foreach($this->tab as $benevole){
            
            if($i==0){
               
                $res .= " <div class='row'>";
               
            }
            if($i<3){

                $res.="<a href='".$this->basePath.'/user/perso/'.$benevole->idBenev."'><div class='col-md-4 mb'><div class='white-panel pn' >
                <div class='white-header'>
                <h5>".$benevole->nom ." ".$benevole->prenom."</h5>
              </div>
              <p><img src='img/".$benevole->image."' class='img-circle' width='50'></p>
              <p><b>Benevole</b></p>
              <div class='row'>
                <div class='col-md-6'>
                  <p class='small mt'>Age</p>
                  <p>".$benevole->age."</p>
                </div>
                <div class='col-md-6'>
                  <p class='small mt'>email</p>
                  <p>".$benevole->email."</p>
                </div>
              </div></div></div></a>";
              $i++;

            }
            else{
                $i=0;
                $res .="</div>";
            }
        }
      
       
       return $res;
     
    }



    public function affichageUser(){
        $res=' <h1>Votre Planning:</h1>';
      
        foreach($this->tab as $bsn){
            $crn = Creneau::where('idCreneau','=',$bsn->idCreneau)->first(); 
            /*
            JOURS
            */
            if($crn->jour == 1)
            $jour = "lundi";
        if($crn->jour == 2)
            $jour = "Mardi";
        if($crn->jour == 3)
            $jour = "Mercredi";
        if($crn->jour == 4)
            $jour = "Jeudi";
        if($crn->jour == 5)
            $jour = "vendredi";
        if($crn->jour == 6)
            $jour = "samedi";
        if($crn->jour == 7)
            $jour = "dimanche";    

            /*
                POSTES
            */
            if($bsn->idPoste == 1)
            $poste = "Caissier titulaire";
        if($bsn->idPoste == 2)
            $poste = "Caissier assistant";
        if($bsn->idPoste == 3)
            $poste = "Gestionnaire de vrac titulaire";
        if($bsn->idPoste == 4)
            $poste = "Gestionnaire de vrac assistant";
        if($bsn->idPoste == 5)
            $poste = "Chargé d'accueil titulaire";
        if($bsn->idPoste == 6)
            $poste = "Chargé d'accueil assistant";
      
            $res.="<div class='row'><div class='col-md-4 mb'><div class='white-panel pn' ><div class='white-header'>
          <h3>".$jour."</h3>
          </div>
          <h6>".$crn->heureDeb."h</h6>
          <h4>".$poste."</h4>
          <h6>".$crn->heureFin."h</h6>
            </div></div></div>";



        }
     




        return $res;
    }
}
