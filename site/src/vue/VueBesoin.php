<?php
namespace coboard\vue;


class VueBesoin extends VueGlobale
{

    protected $id;

    public function __construct($t,$b,$i){
        parent::__construct($t,$b);
        $this->id = $i;
    }

    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 0 :
            {
                $content .= $this->renderBesoin();
                break;
            }
            case 1 :
            {
                $content .= $this->formBesoin();
                break;
            }
            case 2 :
            {
                $content .= $this->formReserv();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    public function renderBesoin(){
        $creneau=$this->id;
        $bouton= '<button onclick="location.href=\'' .$this->basePath."/besoins/creation/$creneau'\"". 'type=\"button\" class=\"btn btn - success\">Creer nouveau besoin</button></td>
            </tr>
          </thead>
          <tbody>';
        $res = "
        <h1>Listes des besoins</h1>
        <table class=\"table\">
          <thead>
            <tr>
              <th scope=\"col\">Poste</th>
              <th scope=\"col\">Nom</th>
              <td>";
        $res.=$bouton;

        foreach ($this->tab as $besoin) {
            $label = $besoin->role;
            $benev = $besoin->user;
            if($besoin->idBenev == 0){
                $personne = '<button onclick="location.href=\'' .$this->basePath."/besoins/reserver/$besoin->idBesoin'\"". 'type=\"button\" class=\"btn btn - success\">Réserver</button>';
            }else{
                $personne = $besoin->idBenev;
            }
                $res .= "<tr>
                         <th scope=\"row\">$label->label</th>
                         <td>$personne</td>
                         </tr>";
            }
        $res .="</tbody>
                </table>";


        return $res;
        }

    public function formBesoin(){
        return "<form action=\"$this->basePath/besoins/creation/$this->id\" method=\"post\">
    <br><select id=\"poste\"  name=\"poste\" class=\"form-control form-control-lg\">
     <option>Caissier titulaire</option>
     <option>Caissier assistant</option>
     <option>Gestionnaire de vrac titulaire</option>
     <option>Gestionnaire de vrac assistant</option>
     <option>Chargé d'accueil titulaire</option>
     <option>Chargé d'accueil assistant</option>
    </select><br>
  <button type=\"submit\" class=\"btn btn-primary\">Valider</button>
</form>";
    }

    public function formReserv(){
        return "<form action=\"$this->basePath/besoins/reserver/$this->id\" method=\"post\">
    <br><select id=\"prenom\"  name=\"prenom\" class=\"form-control form-control-lg\">
     <option>Cassandre</option>
     <option>Achille</option>
     <option>Calypso</option>
     <option>Bacchus</option>
     <option>Diane</option>
     <option>Clark</option>
     <option>Helene</option>
     <option>Jason</option>
     <option>Bruce</option>
     <option>Pénélope</option>
     <option>Ariane</option>
     <option>Lois</option>
    </select><br>
  <button type=\"submit\" class=\"btn btn-primary\">Valider</button>
</form>";
    }

}