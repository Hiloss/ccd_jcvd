<?php
namespace coboard\vue;

class VueCompte extends VueGlobale
{

    public function __contruct(){
        parent::__construct();
    }

    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 0 :
            {
                $content .= $this->renderLoginForm();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    public function renderLoginForm(){
      $res="  <div id='login-page'>
        <div class='container'>
          <form class='form-login' method='POST' action='".$this->basePath."/user/login'>
            <h2 class='form-login-heading'>Connectez-Vous</h2>
            <div class='login-wrap'>
              <input type='text' class='form-control' name='login' placeholder='Login' autofocus>
              <br>
              <input type='password' class='form-control' name='pwd' placeholder='Password'>
             
              <button class='btn btn-theme btn-block' type='submit'><i class='fa fa-lock'></i>Connexion</button>
              <hr>
            </div>
          </form>
        </div>
      </div>";

      return $res;
    } 


}